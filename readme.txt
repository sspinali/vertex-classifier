
# Create a new virtual:
virtualenv -p python2.7 v_env_name

# Activate the environment:
source path/to/env/bin/activate

# Install requirements:
pip install -r requirements.txt

Run the code:
python main.py /path/to/file/file_name.root path/to/output/
